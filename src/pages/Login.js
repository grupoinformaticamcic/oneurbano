import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './styles/Login.css';
import logo from '../logo.png';
//import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import md5 from 'md5';
import Cookies from 'universal-cookie';


const cookies = new Cookies();

class Login extends Component {
    state={
        form:{
            usuario: '',
            contrasena: ''
        }
    }

    handleChange=async e=>{
        await this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        });
    }

    iniciarSesion=async()=>{
        const contrasena1 = md5(this.state.form.contrasena);
        const baseUrl=`https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/Cliente?searchString={"usuario":"${this.state.form.usuario}", "contrasena":"${contrasena1}"}`
        await axios.get(baseUrl)
        .then(response=>{
            return response.data;
        })
        .then(response=>{
            if(response.length>0){
                var respuesta=response[0];
                cookies.set('id', respuesta.id, {path: "/"});
                cookies.set('apellido', respuesta.apellido, {path: "/"});
                cookies.set('nombre', respuesta.nombre, {path: "/"});
                cookies.set('usuario', respuesta.usuario, {path: "/"});
                alert(`Bienvenido ${respuesta.nombre} ${respuesta.apellido}`);
                window.location.href="./Home";
            }else{
                alert('El usuario o la contraseña no son correctos');
            }
        })
        .catch(error=>{
            console.log(error);
        })

    }

    componentDidMount() {
        if(cookies.get('usuario')){
            window.location.href="./Home";
        }
    }
    

    render() {
        return (
    <div className="login">
        <h3>Bienvenido a One Urbano</h3>
        <img src={logo} className="App-logo" alt="logo" />
        <div className="containerPrincipal">
            <div className="containerSecundario">
            <div className="form-group">
                <label>Usuario: </label>
                <br />
                <input
                type="text"
                className="form-control"
                name="usuario"
                onChange={this.handleChange}
                />
                <br />
                <label>Contraseña: </label>
                <br />
                <input
                type="password"
                className="form-control"
                name="contrasena"
                onChange={this.handleChange}
                />
                <br />
                <br />
                <button className="btn btn-primary" onClick={()=> this.iniciarSesion()}>Iniciar Sesión</button>
                <br />
                <br />
                <p className="par">¿No estás registrado? Haz click <Link to = "/Crear_Usuario">Aquí</Link> para registrarte</p>
            </div>
            </div>
        </div>
      </div>
        );
    }
}

export default Login;
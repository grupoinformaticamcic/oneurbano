import React from 'react';
import './styles/Home.css';
import logo from '../logo.png';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';
import Signout from '../components/Signout.js'


const cookies = new Cookies();

class Home extends React.Component {

    componentDidMount() {

    }

    render() {
        
        if(!cookies.get('usuario')){
            window.location.href="./";
        }

        return(

            <div className = "Home">
            <h2>Bienvenido {cookies.get('nombre')} a</h2>
            <img src={logo} className="App-logo" alt="logo" />
            <h4>Selecciona una opción:</h4>
           <Link to="/Consulta_Controladores"><li><button type="button" className="btn btn-primary">Consultar Controladores</button></li></Link>
           {/* <Link to="/Consulta_Actuadores"> <li><button type="button" className="btn btn-primary">Consultar actuadores</button></li></Link> */}
            {/* <li><button type="button" className="btn btn-primary">Alertas</button></li> */}
           {/* <Link to="/Cultivos"><li><button type="button" className="btn btn-primary">Cultivos</button></li></Link> */}
           
           <li><Signout></Signout></li>
            </div>
        )
    }

}

export default Home;
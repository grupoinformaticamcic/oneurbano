import React from 'react';
import PageError from '../components/PageError.js'
import PageLoading from '../components/PageLoading.js'
import './styles/Consulta_Sensores.css'
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class Consulta_Sensores extends React.Component {
    constructor(props) {
        super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
        this.state = { //state is by default an object
         loading: true,
         sensors: undefined,
            error:null

         /**/
        };
     };


     renderTableData() {
        
        return this.state.sensors.map((sensor, index) => {
           const { id, 
                   nombre, 
                   estado,
                   fechainstalacion,
                  } 
                   = sensor //destructuring
           return (
              <tr key={id} >
                  <td> <Link to= {`/Detalles/${id}`}><button type="button" className="btn btn-primary">{id}</button></Link></td>
                 <td>{nombre}</td>
                 <td>{estado}</td>
                 <td>{fechainstalacion}</td>
                 
              </tr>
           )
        })
     }



     renderTableHeader() {
        //let header = Object.keys(this.state.sensors[0])
        let header = ["id", "nombre", "estado", "fechainstalacion"]
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     }

     componentDidMount () {
      if(!cookies.get('usuario')){
         window.location.href="./";
     }
      this.fetchData()
     }

     fetchData = async () => {
     //fetchData = () => {
        this.setState({loading:true, error:null})

        try {
         const response = await fetch('https://oneurbano.uk.r.appspot.com/drcolonia/OneUrbano/1.0.0/controlador?&searchString={}')
         const sensors = await response.json();
         console.log(sensors)
         this.setState({loading: false, sensors:sensors})
        }catch(error){
         this.setState({loading: false, error: error})
        }
     }

     render() {

         if(this.state.loading === true && !this.state.sensors){
            return <PageLoading />;
         }
         if (this.state.error) {
            return <PageError error={this.state.error} />;
          }

        return (
           <div className="Consulta_Sensores">
              <h1 id='title'>Consulta de Controladores</h1>
              <table id='sensors'>
                 <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                 </tbody>
              </table>
              <br />
              <br />
              <Link to= {`/Crear_Controlador`}><button type="button" className="btn btn-primary">Crear Controlador</button></Link>
           </div>
        )
     }

}

export default Consulta_Sensores;
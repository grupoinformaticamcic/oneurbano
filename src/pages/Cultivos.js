import React from 'react';
import MapView from '../components/MapView.js'
import './styles/Cultivos.css'


class Cultivos extends React.Component {
    render() {
        return(
            <div className = "Cultivos">
            <h2>Consulta de Cultivos</h2>
            <MapView></MapView>
            </div>
        )
    }

}

export default Cultivos;
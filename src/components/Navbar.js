import React from 'react';
import './styles/navbar.css'
import { Link } from 'react-router-dom';

import logo from '../logo_no_text.png';

class Navbar extends React.Component {
    render() {
        return(
           
           <Link to=  "/Home" style={{ textDecoration: 'none' }}>
           <div className= "navbar">
            <img src={logo} className="App-logo-navbar" alt="logo" />
            <span>One Urbano</span>
            </div></Link>
        )
    }

}

export default Navbar;
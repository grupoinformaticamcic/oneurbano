import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import { Link } from 'react-router-dom';
import L from 'leaflet';

class MapView extends React.Component {


  render() {
    let LOCALIZACION_SENSOR = [this.props.latitud, this.props.longitud];
    const styleMap = { "width": "95%", "height": "60vh", "borderRadius": "10px" }
    const iconOne = new L.Icon({
        iconUrl: '../logo.png',
        //iconRetinaUrl: '../logo.png',
        iconSize: new L.Point(35, 40),
        //className: 'leaflet-div-icon',
        html: 'transparent'
    });
    
    return (
      <MapContainer
        style={styleMap}
        center={LOCALIZACION_SENSOR}
        zoom={13}>

        <TileLayer
          url="https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png"
          attribution= '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
        />
    <Marker icon={iconOne} position={LOCALIZACION_SENSOR}>
      <Popup>
        <b>Controlador {this.props.id_sensor}</b>
        <p>Para consultar el riego haga click <Link to = {`/Consulta_Riego/${this.props.id_sensor}`}>Aquí</Link></p>
      </Popup>
    </Marker>
      </MapContainer>
    )
  }

}

export default MapView;
import React from 'react';

import './App.css';
import Home from './pages/Home';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Consulta_Sensores from './pages/Consulta_Sensores';
import Consulta_Actuadores from './pages/Consulta_Actuadores';
import Cultivos from './pages/Cultivos';
import Detalles from './pages/Detalles';
import Layout from './components/Layout';
import Login from './pages/Login';
import Crear_Usuario from './pages/Crear_Usuario';
import Crear_Controlador from './pages/Crear_Controlador';
import Consulta_Riego from './pages/Consulta_Riego';

function App() {
  return (
    
   <BrowserRouter>
   <Switch>
     <Layout>
      <Route exact path="/" component={Login} />
      <Route exact path="/Crear_Usuario" component={Crear_Usuario} />
      <Route exact path="/Home" component={Home} />
      <Route exact path="/Consulta_Controladores" component={Consulta_Sensores} />
      <Route exact path="/Crear_Controlador" component={Crear_Controlador} />
      <Route exact path="/Consulta_Riego/:controladorId" component={Consulta_Riego} />
      <Route exact path="/Consulta_Actuadores" component={Consulta_Actuadores} />
      <Route exact path="/Cultivos" component={Cultivos} />
      <Route exact path="/Detalles/:detallesId" component={Detalles} />
      </Layout>
    </Switch>
    </BrowserRouter>
  );
}

export default App;

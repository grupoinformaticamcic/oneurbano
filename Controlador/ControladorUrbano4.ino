#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>
#include "HTTPSRedirect.h"
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

#ifndef STASSID
//#define STASSID "FAMILIANINO."
//#define STAPSK  "Canela20161008*"
#define STASSID "Familia_Ramirez_Valderrama"
#define STAPSK  "D4n13lBrUn0R1ng0J4gg3r"
#endif

const char* ssid     = STASSID;
const char* password = STAPSK;

const char* host = "oneurbano.uk.r.appspot.com";
String url = "/drcolonia/OneUrbano/1.0.0/Mediciones";
String url2 = "/drcolonia/OneUrbano/1.0.0/Riego";
const int httpsPort = 443;
const char* fingerprint = "80 F2 A2 C1 BB 8F 2B 15 B8 78 4C 67 37 1E CF 3D BD 09 B6 53";
const char* fingerprint2 = "B8 74 FC 33 93 10 99 50 78 46 95 D8 8A AF 4C E7 7E 27 EB 10";

Adafruit_BMP280 bmp; 
HTTPSRedirect* client = nullptr;
float Temperature, Humedity, Pressure;
int id =  1;
int id2 =  1;
String idcont = "Urbano4"; //*****modificar******
const int bomba = 14; //D5

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "0.south-america.pool.ntp.org",-18000,6000);

SoftwareSerial GPSModule(13, 15); // RX, TX

int updates;
int failedUpdates;
int pos;
int stringplace = 0;

String timeUp;
String nmea[15];

void setup() {
  Serial.begin(115200);

  bmp.begin(0x76);
  pinMode(bomba, OUTPUT);
  timeClient.begin();
  GPSModule.begin(9600);
   
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);


  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void loop() {

    //Temperature

    Serial.print(bmp.readTemperature());
    Temperature = bmp.readTemperature();
    
    Serial.println();
    delay(1000);

    //Pressure
   
    Serial.print(bmp.readPressure());
    Pressure = bmp.readPressure();
    Serial.println();
    delay(1000);

    //Humedity

    int lectura = analogRead(A0);
    int lecturaPorcentaje = map(lectura, 1023, 500, 0, 100);
    Serial.println(lecturaPorcentaje);
    Humedity = lecturaPorcentaje;

    Serial.println();
    delay(1000);
  
    //GPS

    String Lati, Long; //****MODIFICAR****
    Lati = "4.635944"; //****MODIFICAR****
    Long = "-74.098902"; //****MODIFICAR****
    Serial.println("Latitud: " + Lati + " Longitud: " + Long);


    // Date

     String formattedDate;

      String Date, Time, DateTime;
      timeClient.update();
      formattedDate = timeClient.getFormattedDate();
      Date = formattedDate.substring(0, formattedDate.indexOf("T"));
      Time = formattedDate.substring(formattedDate.indexOf("T") + 1, formattedDate.length()-1);
      DateTime = Date + " " + Time;
      Serial.println(DateTime);
    
      delay(1000);

    
      
  // Send data
  
  client = new HTTPSRedirect(httpsPort);
  client->setInsecure();
  client->setPrintResponseBody(true);
  client->setContentTypeHeader("application/json");
  
  Serial.print("Connecting to ");
  Serial.println(host);

  // Try to connect for a maximum of 5 times
  bool flag = false;
  for (int i=0; i<5; i++){
    int retval = client->connect(host, httpsPort);
    if (retval == 1) {
       flag = true;
       break;
    }
    else
      Serial.println("Connection failed. Retrying...");
  }

  if (!flag){
    Serial.print("Could not connect to server: ");
    Serial.println(host);
    Serial.println("Exiting...");
   // return;
  }else{
    Serial.println("Connection OK...");
  }

  
  if (client->setFingerprint(fingerprint)) {
    Serial.println("Certificate match.");
  } else {
    Serial.println("Certificate mis-match");
  }

  String datasend =  "{\"Mediciones\":{\"id\": \"" + (String)id + "\",\"Temperatura\": \"" + (String)Temperature + "\",\"Presion\": \"" + (String)Pressure + "\",\"Humedad\": \"" + (String)Humedity + "\",\"Latitud\": \"" + (String)Lati + "\",\"Longitud\": \"" + (String)Long + "\",\"fechaMedicion\": \"" + DateTime + "\",\"controlador\":{\"id\": \"" + (String)idcont + "\",\"nombre\": \"pariatur sed\",\"estado\": \"sed\",\"fechainstalacion\": \"1979-04-03T14:29:43.909Z\",\"propietario\": {\"id\": 1231546,\"nombre\": \"Sergio\",\"apellido\": \"Rami\",\"usuario\": \"ser\",\"contrasena\": \"rrrrm\"}}}}";
  client->POST(url, host, datasend, true); 
  
  id = id +1;
  
  //delay(10000);  


   
   String respuesta = client->getResponseBody();

  delete client;
  client = nullptr;

 
  

   Serial.println(respuesta);
    if(respuesta.substring(0,1).equals("1")){
      Serial.println ("Activar actuador");
      digitalWrite(bomba, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(3000);
      digitalWrite(bomba, LOW);    // turn the LED off by making the voltage LOW
      String TimeR = "3 s"; 
      
       // Send data
  
        client = new HTTPSRedirect(httpsPort);
        client->setInsecure();
        client->setPrintResponseBody(true);
        client->setContentTypeHeader("application/json");
        
        Serial.print("Connecting to ");
        Serial.println(host);
      
        // Try to connect for a maximum of 5 times
        bool flag2 = false;
        for (int i=0; i<5; i++){
          int retval = client->connect(host, httpsPort);
          if (retval == 1) {
             flag2 = true;
             break;
          }
          else
            Serial.println("Connection failed. Retrying...");
        }
      
        if (!flag2){
          Serial.print("Could not connect to server: ");
          Serial.println(host);
          Serial.println("Exiting...");
         // return;
        }else{
          Serial.println("Connection OK...");
        }
      
        
        if (client->setFingerprint(fingerprint2)) {
          Serial.println("Certificate match.");
        } else {
          Serial.println("Certificate mis-match");
        }
    
      
      
      String datasend2 =  "{\"Riego\":{\"id\": \"" + (String)id2 + "\",\"Tiempo\": \"" + (String)TimeR + "\",\"fechaRiego\": \"" + DateTime + "\",\"actuador\":{\"id\": \"" + (String)idcont + "\",\"nombre\": \"pariatur sed\",\"estado\": \"sed\",\"fechainstalacion\": \"1979-04-03T14:29:43.909Z\",\"propietario\": {\"id\": 1231546,\"nombre\": \"Sergio\",\"apellido\": \"Rami\",\"usuario\": \"ser\",\"contrasena\": \"rrrrm\"}}}}";
      client->POST(url2, host, datasend2, true); 
      id2 = id2 +1;
      delay(1000); 
    }else{//0
      Serial.println ("Desactivar actuador");
      digitalWrite(bomba, LOW);    // turn the LED off by making the voltage LOW
      delay(3000); 
    }
    
 
  // delete HTTPSRedirect object
  delete client;
  client = nullptr;

  //delay(10000);//periodo para cuanto toma 
}

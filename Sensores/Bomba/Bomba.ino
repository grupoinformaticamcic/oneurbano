
const int bomba = 14;

void setup() {

  pinMode(bomba, OUTPUT);
}

void loop() {
  digitalWrite(bomba, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(bomba, LOW);    // turn the LED off by making the voltage LOW
  delay(3000);                       // wait for a second
}

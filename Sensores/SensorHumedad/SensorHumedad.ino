void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
int lectura = analogRead(A0);

Serial.print("La lectura es: ");
Serial.println(lectura);
//lecturas entre 1000 y 1023
//if (lectura >= 1000){
//  Serial.println(">> El sensor está desconectado o fuera del suelo <<");
//  }
//  else if (lectura <1000 && lectura >= 600){
//    Serial.println(">> El suelo está seco <<");
//      }
//  else if (lectura <600 && lectura >= 370){
//     Serial.println(">> El suelo está humedo <<");
//  }
//  else if (lectura < 370){
//     Serial.println(">> El sensor está en agua <<");
//  }
  delay(1000);

  //convertir a porcentaje
  int lecturaPorcentaje = map(lectura, 1023, 430, 0, 100);
  Serial.println("La humedad del suelo es: ");
  Serial.println(lecturaPorcentaje);
  Serial.println("%");
}

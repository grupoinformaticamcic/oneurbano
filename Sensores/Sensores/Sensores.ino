#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#define PRESIONNIVELDELMAR_HPA (1013.25)

Adafruit_BMP280 bmp; 

float temperature, humedity, pressure, altitude;

void setup() {
  Serial.begin(9600);
  delay(100);

  bmp.begin(0x76);
}
void loop() {
    Serial.print(F("Temperature = "));
    Serial.print(bmp.readTemperature());
    Serial.println(" °C");

    Serial.print(F("Pressure = "));
    Serial.print(bmp.readPressure());
    Serial.println(" Pa");

    Serial.print(F("Approx altitude = "));
    Serial.print(bmp.readAltitude(1013.25)); /* Adjusted to local forecast! */
    Serial.println(" m");

    Serial.println();
    delay(2000);

int lectura = analogRead(A0);

Serial.print("La lectura es: ");
Serial.println(lectura);
//lecturas entre 1000 y 1023
//if (lectura >= 1000){
//  Serial.println(">> El sensor está desconectado o fuera del suelo <<");
//  }
//  else if (lectura <1000 && lectura >= 600){
//    Serial.println(">> El suelo está seco <<");
//      }
//  else if (lectura <600 && lectura >= 370){
//     Serial.println(">> El suelo está humedo <<");
//  }
//  else if (lectura < 370){
//     Serial.println(">> El sensor está en agua <<");
//  }
  delay(1000);

  //convertir a porcentaje
  int lecturaPorcentaje = map(lectura, 1023, 0, 0, 100);
  Serial.println("La humedad del suelo es: ");
  Serial.println(lecturaPorcentaje);
  Serial.println("%");
    
}
